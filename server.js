const express = require('express');
const Contenedor = require('./classes/Contenedor.js');
const Mensajes = require('./classes/mensajes.js');
const handlebars = require('express-handlebars');

//Defino variables
let contenedor = new Contenedor('productos.json');
//Defino variables

let mensajes =  new Mensajes();
const app = express();
const puerto = 8080;


// 
const http = require('http').Server(app)
const io = require('socket.io')(http);


// incorporo el router
const router_api = express.Router();
const router = express.Router();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//defino ruta base
app.use('/api/productos', router_api);
app.use('/productos', router);
// indico donde estan los archivos estaticos
app.use(express.static('public'));
// configuracion de handlebars en express
app.engine('hbs', handlebars({
    extname: '.hbs',
    defaultLayout: 'index.html',
}));
// seteo el motor de plantilla
app.set('view engine', 'hbs');
app.set('views', './views');

// pongo a escuchar el servidor en el puerto indicado
const server = http.listen(puerto, () => {
   //console.log(`servidor escuchando en http://localhost:${puerto}`);
   console.log("Iniciando servidor")   
   //console.log(obj_items)
   console.log( "servidor inicializado en puerto:" + server.address().port)
});

// en caso de error, avisar
server.on('error', error => {
    console.log('error en el servidor:', error);
});

/*******************************/
/*      SOCKET                 */
/*******************************/
io.on('connection',(socket)=>{
    console.log('Usuario conectado :' + socket.id);
    socket.emit('getConnection','Bienvenido, conectado con Id:'+socket.id);
    socket.emit('getProductos',contenedor.getAllSync());
    socket.emit('getMensajes',mensajes.listAll());
    
    socket.on('new-message', data => {
        console.log('Mensaje recibido de :' + socket.id);
        //Guardo el mensaje
        mensajes.addMessage(data.chat_user,data.chat_text)
        notifyMensajes();
    });
});


//notifico los productos a todos los sockets
function notifyProductos(){
    io.sockets.emit('getProductos',contenedor.getAllSync())
}

//notifico los mensajes a todos los sockets
function notifyMensajes(){
    io.sockets.emit('getMensajes',mensajes.listAll())
    console.log("emito_mensajes");
}


/********************/
/*       WEB        */
/********************/
router.get('/', (req, res) => {   
    res.render('live', { productos: contenedor.getAllSync(), hasProductos: (contenedor.getProductsCount() > 0) });
});

router.get('/vista', (req, res) => {   
    res.render('vista', { productos: contenedor.getAllSync(), hasProductos: (contenedor.getProductsCount() > 0) });
});

router.get('/grabar', (req, res) => {   
    res.render('grabar', null);
});


/********************/
/*       API        */
/********************/
//TEST
router_api.get('/test', (req, res) => {
    res.send('get recibido!');
});

router_api.post('/test', (req, res) => {
    res.send('post recibido!')
});

//LISTAR
router_api.get('/listar', (req , res)=> {
    //listo todos los productos
    let result = contenedor.getAllSync();
    if (result) {
        console.log(result)
        res.send(result);
    } else {
        res.status(500).json({ error: 'no se pueden obtener los productos' });
    }
} );

//LISTAR UNO
router_api.get('/listar/:id', (req , res)=> {
 //LISTO SOLO 1 Elemento    
        //LISTO SOLO 1 Elemento    
        let result = contenedor.getByIdSync(req.params.id)
        if (result) {
            res.json(result);
        } else {
            res.status(500).json({ error: 'producto no encontrado' });
        }
} );

// CARGA PRODUCTOS
router_api.post('/guardar', (req , res)=> {
    if (Array.isArray(req.body)) {
        let added_products = [];
        let json_array = req.body.map(JSON.stringify);
        json_array.forEach(it => {
            let producto = JSON.parse(it);
            let response = contenedor.saveSync(producto)
            added_products.push(response);

        });
        if (added_products.length > 0) {
            //res.json(added_products)
            res.render('grabar', null);
        }
        else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    } else {
        let result = contenedor.saveSync(req.body)
        if (result) {
            //res.json(result)
            res.render('grabar', null);
        } else {
            res.status(500).json({ resultado: 'No se puede guardar el producto encontrado' })
        }

    }
    //notifico los productos a todos los clientes
    notifyProductos();
} )

// ACTUALIZA PRODUCTOS
router_api.put('/:id', (req , res)=> {
    let result = contenedor.updateByIdSync(req.params.id, req.body);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' actualizado' })
    } else {
        res.status(500).json({ error: 'producto no encontrado' });
    }
} )

// ELIMINA PRODUCTOS
router_api.delete('/:id', (req , res)=> {
    let result = contenedor.deleteByIdSync(req.params.id);
    if (result) {
        res.json({ resultado: 'id: ' + req.params.id + ' eliminado' })
    } else {
        res.status(500).json({ resultado: 'id:' + req.params.id + ' No enocontrado' })
    }
} )