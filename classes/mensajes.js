
const fs =require('fs');

class Mensajes{

    constructor(){
        this.arr_mensajes=[];
        this.nextId=0;

        try{
            let file_content=fs.readFileSync('./mensajes.json','utf-8')
            this.arr_mensajes= JSON.parse(file_content)
            //console.log(arr_products)
        
            }catch(err){
                console.log('No se encontraron mensajes :' + err)
            }

    }

    getNextId(){
        return ++this.nextId;
    }

    listAll(){
        if(this.arr_mensajes){
            return this.arr_mensajes;
        }
        else{
            return({info:'No hay mensajes.'});
        }
    }

    addMessage(user,text){       
        let msg={}
        msg.chat_user=user;
        msg.chat_text=text;
        msg.chat_date=new Date().toLocaleString('ddMMyyy');
        msg.chat_id=this.getNextId()
   
            console.log("guardando Mensaje: " + JSON.stringify(msg))
            this.arr_mensajes.push(msg);
        fs.writeFileSync('./mensajes.json',JSON.stringify(this.arr_mensajes))    
        return(msg);
    }

    //TODO Guardar en archivo
    //TODO LEER EL ARCHIVO DE MENSAJES

}

module.exports = Mensajes